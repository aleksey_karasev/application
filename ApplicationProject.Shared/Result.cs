﻿namespace ApplicationProject.Shared
{
    /// <summary>
    /// Class describing the result
    /// </summary>
    public class Result<T>
    {
        public bool Success { get; }
        public T Data { get; }
        public string Message { get; }

        private Result(bool success, T data, string message)
        {
            Success = success;
            Data = data;
            Message = message;
        }

        public static Result<T> Ok(T data, string message = "Ok")
        {
            return new Result<T>(true, data, message);
        }

        public static Result<T> Fail(string errorMessage)
        {
            return new Result<T>(false, default, errorMessage);
        }
    }
}