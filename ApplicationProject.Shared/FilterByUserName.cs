﻿
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ApplicationProject.Shared
{
    public class FilterByUserName
    {
        [BindNever]
        public string UserName { get; set; } = "user";
        [BindNever]
        public string RoleName { get; set; } = "role";
    }
}
