﻿using System.ComponentModel;

namespace ApplicationProject.Shared
{
    public class BaseFilter : FilterByUserName
    {

        public DateTime? CreatedAfter { get; set; }
        public DateTime? CreatedBefore { get; set; }

        public DateTime? SubmitAfter { get; set; }
        public DateTime? SubmitBefore { get; set; }

        public string? SortBy { get; set; }

        [DefaultValue(true)]
        public bool IsSortAscending { get; set; }
    }
}
