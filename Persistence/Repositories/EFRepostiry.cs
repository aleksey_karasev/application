﻿using Microsoft.EntityFrameworkCore;
using ApplicationProject.Application.Interfaces.Repositories;
using ApplicationProject.Domain.Abstractions;

namespace ApplicationProject.Persistence.Repositories
{
    public class EFRepostiry<T> : IRepository<T> where T : Entity
    {
        private readonly DbContext _context;
        private readonly DbSet<T> _dbSet;

        public EFRepostiry(DbContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public IQueryable<T> Entities => _dbSet;

        public T Add(T entity)
        {
            var newEntity = _dbSet.Add(entity);
            return newEntity.Entity;
        }

        public async Task<T> AddAsync(T entity)
        {
            var newEntity = await _dbSet.AddAsync(entity);
            return newEntity.Entity;
        }

        public void AddRange(ICollection<T> entities)
        {
            _dbSet.AddRange(entities);
        }

        public async Task AddRangeAsync(ICollection<T> entities)
        {
            await _dbSet.AddRangeAsync(entities);
        }

        public void Delete(T entity)
        {
            _dbSet.Remove(entity);
        }

        public T Get(Guid id)
        {
            return _dbSet.FirstOrDefault(f => f.Id == id);
        }

        public IQueryable<T> GetAll()
        {
            return _dbSet.AsNoTracking();
        }
        
        public async Task<ICollection<T>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<T> GetAsync(Guid id)
        {
            return await _dbSet.FirstOrDefaultAsync(f => f.Id == id);
        }

        public T Update(T entity)
        {
            var res = _dbSet.Update(entity);
            return res.Entity;
        }

    }
}
