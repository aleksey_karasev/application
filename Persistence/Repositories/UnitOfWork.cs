﻿using Microsoft.EntityFrameworkCore;
using ApplicationProject.Application.Interfaces.Repositories;
using ApplicationProject.Domain.Abstractions;

namespace ApplicationProject.Persistence.Repositories
{
    public class UnitOfWork<TContext> : IUnitOfWork where TContext : DbContext
    {
        private readonly Dictionary<Type, object> _repositories = new Dictionary<Type, object>();
        private readonly TContext _context;

        public UnitOfWork(TContext context)
        {
            this._context = context;
        }

        public IRepository<T> GetRepository<T>() where T : Entity
        {
            var type = typeof(T);
            if (!_repositories.ContainsKey(type))
            {
                _repositories[type] = new EFRepostiry<T>(_context);
            }

            return (IRepository<T>)_repositories[type];
        }

        public int SaveChanges(CancellationToken cancellationToken)
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync();
        }
    }
}
