﻿using Microsoft.EntityFrameworkCore;
using ApplicationProject.Domain.Entities;

namespace ApplicationProject.Persistence.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<ApplicationСonference> Applications { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            #region "Validation in Database"

            modelBuilder.Entity<ApplicationСonference>()
                .Property(e => e.Id).IsRequired();

            modelBuilder.Entity<ApplicationСonference>()
                .Property(e => e.AuthorId).IsRequired();

            modelBuilder.Entity<ApplicationСonference>()
                .Property(e => e.Name).HasMaxLength(100);

            modelBuilder.Entity<ApplicationСonference>()
                .Property(e => e.Description).HasMaxLength(300);

            modelBuilder.Entity<ApplicationСonference>()
                .Property(e => e.Plan).HasMaxLength(1000);

            #endregion

            base.OnModelCreating(modelBuilder);
        }
    }
}
