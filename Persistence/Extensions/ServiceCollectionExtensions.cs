﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System.Text.Json.Serialization;

using ApplicationProject.Application.Interfaces.Repositories;
using ApplicationProject.Persistence.Context;
using ApplicationProject.Persistence.Repositories;
using ApplicationProject.Persistence.Settings;

namespace ApplicationProject.Persistence.Extensions
{
    /// <summary>
    /// The class for connecting services from the persistence layer
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddPersistenceServices(this IServiceCollection services, IConfiguration configuration)
        {
            //Created a project settings class in advance ApplicationSettings
            var applicationSettings = configuration.Get<ApplicationSettings>();

            // Postgres Password is written in secrets
            var connectionString = string.Format(applicationSettings.ConnectionString, applicationSettings.DbPassword);

            //Connection string for the database and other services
            services.AddSingleton(applicationSettings)
                    .AddSingleton((IConfigurationRoot)configuration)
                    .AddDbContext(connectionString)
                    .AddUnitOfWork()
                    .AddJson()
                    .AddAuthenticationConfig()
                    .AddSwagger()
                    .AddServices();

            return services;
        }

        public static IServiceCollection AddDbContext(this IServiceCollection services,
            string connectionString)
        {
            services.AddDbContext<ApplicationDbContext>(options
                => options
                .UseLazyLoadingProxies()
                .UseNpgsql(connectionString));

            return services;
        }

        public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork<ApplicationDbContext>>();

            return services;
        }

        public static IServiceCollection AddJson(this IServiceCollection services)
        {

            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                });

            return services;
        }

        private static IServiceCollection AddAuthenticationConfig(this IServiceCollection services)
        {
            services.AddAuthentication();
            services.AddAuthorization();

            services.AddCors(opt =>
            {
                opt.AddPolicy("AllowAll", policy =>
                {
                    policy.AllowAnyHeader();
                    policy.AllowAnyMethod();
                    policy.AllowAnyOrigin();
                });
            });

            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen();

            return services;
        }

        private static IServiceCollection AddServices(this IServiceCollection services)
        {
            return services;
        }
    }
}
