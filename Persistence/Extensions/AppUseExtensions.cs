﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;

namespace ApplicationProject.Persistence.Extensions
{
    public static class AppUseExtensions
    {
        public static void UseAppExtensions(this WebApplication app)
        {
            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyHeader()
                       .AllowAnyMethod();
            });

            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllers();
        }
    }
}
