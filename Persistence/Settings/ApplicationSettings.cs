﻿
namespace ApplicationProject.Persistence.Settings
{
    public class ApplicationSettings
    {
        public string ConnectionString { get; set; }
        public string DbPassword { get; set; }
    }
}
