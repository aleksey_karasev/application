﻿
namespace ApplicationProject.Domain.Enums
{
    public enum TypeOfActivity
    {
        Report = 1,
        Masterclass = 2,
        Discussion = 3
    }
}
