﻿

using System.ComponentModel.DataAnnotations;

namespace ApplicationProject.Domain.Abstractions
{
    public abstract class Entity : IEntity
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? SubmitDate { get; set; }
    }
}
