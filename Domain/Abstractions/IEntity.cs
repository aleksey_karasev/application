﻿
namespace ApplicationProject.Domain.Abstractions
{
    public interface IEntity
    {
        public Guid Id { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? SubmitDate { get; set; }
    }
}
