﻿using ApplicationProject.Domain.Abstractions;
using ApplicationProject.Domain.Enums;

namespace ApplicationProject.Domain.Entities
{
    public class ApplicationСonference : Entity
    {
        public Guid AuthorId { get; set; } 

        public TypeOfActivity Type { get; set; }

        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? Plan { get; set; }
    }
}
