﻿
using ApplicationProject.Domain.Entities;
using ApplicationProject.Tests.Common;

namespace ApplicationProject.Tests.ApplicationTests.Queries
{
    public class GetCurrentApplicationForUserQueriesTests : TestBase
    {
        [Fact]
        public async Task GetCurrentApplication_WhenApplicationSubmitted_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            //Act
            var response = await handler.GetCurrentApplicationForUser(ContextFactory.UserIdSubmittedQuery);

            //Assert
            Assert.False(response.Success);
        }

        [Fact]
        public async Task GetCurrentApplication_WhenApplicationUnsubmitted_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            //Act
            var response = await handler.GetCurrentApplicationForUser(ContextFactory.UserIdQuery);

            //Assert
            Assert.True(response.Success);
        }
    }
}
