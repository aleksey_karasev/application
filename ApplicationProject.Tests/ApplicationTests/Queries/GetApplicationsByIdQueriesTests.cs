﻿
using ApplicationProject.Domain.Entities;
using ApplicationProject.Tests.Common;

namespace ApplicationProject.Tests.ApplicationTests.Queries
{
    public class GetApplicationsByIdQueriesTests : TestBase
    {
        [Fact]
        public async Task GetApplication_WhenApplicationNotSubmitted_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            //Act
            var response = await handler.GetApplicationById(ContextFactory.ApplicationIdForQuery);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task GetApplication_WhenApplicationSubmitted_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            //Act
            var response = await handler.GetApplicationById(ContextFactory.ApplicationSubmitedIdForQuery);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task GetApplication_WhenIdRandom_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            //Act
            var response = await handler.GetApplicationById(Guid.NewGuid());

            //Assert
            Assert.False(response.Success);
        }
    }
}
