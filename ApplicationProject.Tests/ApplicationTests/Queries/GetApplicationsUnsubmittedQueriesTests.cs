﻿

using ApplicationProject.Domain.Entities;
using ApplicationProject.Tests.Common;

namespace ApplicationProject.Tests.ApplicationTests.Queries
{
    public class GetApplicationsUnsubmittedQueriesTests : TestBase
    {
        [Fact]
        public async Task GetApplicationUnsubmitted_WhenDateOlderThan20100101_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            //Act
            var response = await handler.GetApplicationsUnsubmittedOlderThan(new DateTime(2010, 1, 2));

            //Assert
            Assert.True(response.Success);
            Assert.Equal(response?.Data.Count, 3);
        }

        [Fact]
        public async Task GetApplicationUnsubmitted_WhenDateOlderThan20200101_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            //Act
            var response = await handler.GetApplicationsUnsubmittedOlderThan(new DateTime(2001, 1, 2));

            //Assert
            Assert.True(response.Success);
            Assert.Equal(response?.Data.Count, 0);
        }
    }
}
