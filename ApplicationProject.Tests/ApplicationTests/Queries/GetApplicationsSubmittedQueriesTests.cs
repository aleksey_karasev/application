﻿
using ApplicationProject.Domain.Entities;
using ApplicationProject.Tests.Common;

namespace ApplicationProject.Tests.ApplicationTests.Queries
{
    public class GetApplicationsSubmittedQueriesTests : TestBase
    {
        [Fact]
        public async Task GetApplicationSubmitted_WhenDateAfter20100101_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            //Act
            var response = await handler.GetApplicationsSubmittedAfter(new DateTime(2010, 1, 1));

            //Assert
            Assert.True(response.Success);
            Assert.Equal(response?.Data.Count, 3);
        }

        [Fact]
        public async Task GetApplicationSubmitted_WhenDateAfter20300101_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            //Act
            var response = await handler.GetApplicationsSubmittedAfter(new DateTime(2030, 1, 1));

            //Assert
            Assert.True(response.Success);
        }
    }
}
