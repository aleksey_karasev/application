﻿using ApplicationProject.Application.DTOs.ApplicationСonferences;
using ApplicationProject.Domain.Entities;
using ApplicationProject.Domain.Enums;
using ApplicationProject.Tests.Common;

namespace ApplicationProject.Tests.ApplicationTests.Commands
{
    public class CreateApplicationCommandsTests : TestBase
    {

        [Fact]
        public async Task CreateApplication_WhenAllFildsFull_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationCreate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = "TestCreate",
                Plan = "TestCreate",
                Name = "TestCreate"
            };

            //Act
            var response = await handler.CreateApplication(applicationCreate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task CreateApplication_WhenAuthorIsNull_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationCreate = new ApplicationСonferenceRequestDTO()
            {
                Type = TypeOfActivity.Report,
                Description = "TestCreate",
                Plan = "TestCreate",
                Name = "TestCreate"
            };

            //Act
            var response = await handler.CreateApplication(applicationCreate, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }

        [Fact]
        public async Task CreateApplication_WhenOnlyAuthorNotNull_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationCreate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
            };

            //Act
            var response = await handler.CreateApplication(applicationCreate, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }

        [Fact]
        public async Task CreateApplication_WhenAuthorAndDescriptionNotNull_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationCreate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Description = "TestCreate",
            };

            //Act
            var response = await handler.CreateApplication(applicationCreate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task CreateApplication_WhenAuthorAndPlanNotNull_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationCreate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Plan = "TestCreate",
            };

            //Act
            var response = await handler.CreateApplication(applicationCreate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task CreateApplication_WhenAuthorAndNameNotNull_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationCreate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Name = "TestCreate",
            };

            //Act
            var response = await handler.CreateApplication(applicationCreate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task CreateApplication_WhenAuthorAndTypeNotNull_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationCreate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
            };

            //Act
            var response = await handler.CreateApplication(applicationCreate, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }

        [Fact]
        public async Task CreateApplication_WhenDescriptionMore300_ShouldReturnSuccessTrue()
        {
            string stringMore = GenerateString(301);

            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationCreate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = stringMore,
                Plan = "TestCreate",
                Name = "TestCreate"
            };

            //Act
            var response = await handler.CreateApplication(applicationCreate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task CreateApplication_WhenPlanMore1000_ShouldReturnSuccessTrue()
        {
            string stringMore = GenerateString(1001);

            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationCreate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = "TestCreate",
                Plan = stringMore,
                Name = "TestCreate"
            };

            //Act
            var response = await handler.CreateApplication(applicationCreate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task CreateApplication_WhenNameMore100_ShouldReturnSuccessTrue()
        {
            string stringMore = GenerateString(101);

            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationCreate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = "TestCreate",
                Plan = "TestCreate",
                Name = stringMore
            };

            //Act
            var response = await handler.CreateApplication(applicationCreate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

    }
}
