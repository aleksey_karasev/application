﻿

using ApplicationProject.Application.DTOs.ApplicationСonferences;
using ApplicationProject.Domain.Entities;
using ApplicationProject.Domain.Enums;
using ApplicationProject.Tests.Common;

namespace ApplicationProject.Tests.ApplicationTests.Commands
{
    public class UpdateApplicationCommandsTests : TestBase
    {
        [Fact]
        public async Task UpdateApplication_WhenAllFildsFull_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationUpdate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = "TestUpdate",
                Plan = "TestUpdate",
                Name = "TestUpdate"
            };

            //Act
            var response = await handler.UpdateApplication(ContextFactory.ApplicationIdForUpdate, applicationUpdate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task UpdateApplication_WhenAuthorIsNull_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationUpdate = new ApplicationСonferenceRequestDTO()
            {
                Type = TypeOfActivity.Report,
                Description = "TestUpdate",
                Plan = "TestUpdate",
                Name = "TestUpdate"
            };

            //Act
            var response = await handler.UpdateApplication(ContextFactory.ApplicationIdForUpdate, applicationUpdate, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }

        [Fact]
        public async Task UpdateApplication_WhenOnlyAuthorNotNull_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationUpdate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
            };

            //Act
            var response = await handler.UpdateApplication(ContextFactory.ApplicationIdForUpdate, applicationUpdate, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }

        [Fact]
        public async Task UpdateApplication_WhenAuthorAndDescriptionNotNull_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationUpdate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Description = "TestUpdate",
            };

            //Act
            var response = await handler.UpdateApplication(ContextFactory.ApplicationIdForUpdate, applicationUpdate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task UpdateApplication_WhenAuthorAndPlanNotNull_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationUpdate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Plan = "TestUpdate",
            };

            //Act
            var response = await handler.UpdateApplication(ContextFactory.ApplicationIdForUpdate, applicationUpdate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task UpdateApplication_WhenAuthorAndNameNotNull_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationUpdate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Name = "TestUpdate",
            };

            //Act
            var response = await handler.UpdateApplication(ContextFactory.ApplicationIdForUpdate, applicationUpdate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task UpdateApplication_WhenAuthorAndTypeNotNull_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationUpdate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
            };

            //Act
            var response = await handler.UpdateApplication(ContextFactory.ApplicationIdForUpdate, applicationUpdate, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }

        [Fact]
        public async Task UpdateApplication_WhenDescriptionMore300_ShouldReturnSuccessTrue()
        {
            string stringMore = GenerateString(301);

            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationUpdate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = stringMore,
                Plan = "TestUpdate",
                Name = "TestUpdate"
            };

            //Act
            var response = await handler.UpdateApplication(ContextFactory.ApplicationIdForUpdate, applicationUpdate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task UpdateApplication_WhenPlanMore1000_ShouldReturnSuccessTrue()
        {
            string stringMore = GenerateString(1001);

            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationUpdate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = "TestUpdate",
                Plan = stringMore,
                Name = "TestUpdate"
            };

            //Act
            var response = await handler.UpdateApplication(ContextFactory.ApplicationIdForUpdate, applicationUpdate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task UpdateApplication_WhenNameMore100_ShouldReturnSuccessTrue()
        {
            string stringMore = GenerateString(101);

            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationUpdate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = "TestUpdate",
                Plan = "TestUpdate",
                Name = stringMore
            };

            //Act
            var response = await handler.UpdateApplication(ContextFactory.ApplicationIdForUpdate, applicationUpdate, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task UpdateApplication_WhenSubmitDateIsNotNull_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationUpdate = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = "TestUpdate",
                Plan = "TestUpdate",
                Name = "TestUpdate"
            };

            //Act
            var response = await handler.UpdateApplication(ContextFactory.ApplicationSubmitedIdForUpdate, applicationUpdate, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }
    }
}
