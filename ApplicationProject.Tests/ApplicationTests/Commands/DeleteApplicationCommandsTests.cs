﻿
using ApplicationProject.Domain.Entities;
using ApplicationProject.Tests.Common;

namespace ApplicationProject.Tests.ApplicationTests.Commands
{
    public class DeleteApplicationCommandsTests : TestBase
    {
        
        [Fact]
        public async Task DeleteApplication_WhenSubmitDateIsNull_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            //Act
            var response = await handler.DeleteApplication(ContextFactory.ApplicationIdForDelete, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task DeleteApplication_WhenSubmitDateIsNotNull_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            //Act
            var response = await handler.DeleteApplication(ContextFactory.ApplicationSubmitedIdForDelete, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }
    }
}
