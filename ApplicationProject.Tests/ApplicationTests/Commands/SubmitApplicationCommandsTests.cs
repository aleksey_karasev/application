﻿using ApplicationProject.Application.DTOs.ApplicationСonferences;
using ApplicationProject.Domain.Entities;
using ApplicationProject.Domain.Enums;
using ApplicationProject.Tests.Common;

namespace ApplicationProject.Tests.ApplicationTests.Commands
{
    public class SubmitApplicationCommandsTests : TestBase
    {

        [Fact]
        public async Task SubmitApplication_WhenAllFildsFull_ShouldReturnSuccessTrue()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationSubmit = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = "TestSubmit",
                Plan = "TestSubmit",
                Name = "TestSubmit"
            };

            //Act
            var data = await handler.CreateApplication(applicationSubmit, CancellationToken.None);

            var response = await handler.SubmitApplication(data.Data.Id, CancellationToken.None);

            //Assert
            Assert.True(response.Success);
        }

        [Fact]
        public async Task SubmitApplication_WhenAuthorAndDescriptionNotNull_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationSubmit = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Description = "TestSubmit",
            };

            var data = await handler.CreateApplication(applicationSubmit, CancellationToken.None);

            //Act
            var response = await handler.SubmitApplication(data.Data.Id, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }

        [Fact]
        public async Task SubmitApplication_WhenAuthorAndPlanNotNull_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationSubmit = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Plan = "TestSubmit",
            };

            var data = await handler.CreateApplication(applicationSubmit, CancellationToken.None);

            //Act
            var response = await handler.SubmitApplication(data.Data.Id, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }

        [Fact]
        public async Task SubmitApplication_WhenAuthorAndNameNotNull_ShouldReturnSuccessFalse()
        {
            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationSubmit = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Name = "TestSubmit",
            };

            var data = await handler.CreateApplication(applicationSubmit, CancellationToken.None);

            //Act
            var response = await handler.SubmitApplication(data.Data.Id, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }

        [Fact]
        public async Task SubmitApplication_WhenDescriptionMore300_ShouldReturnSuccessFalse()
        {
            string stringMore = GenerateString(301);

            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationSubmit = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = stringMore,
                Plan = "TestSubmit",
                Name = "TestSubmit"
            };

            var data = await handler.CreateApplication(applicationSubmit, CancellationToken.None);

            //Act
            var response = await handler.SubmitApplication(data.Data.Id, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }

        [Fact]
        public async Task SubmitApplication_WhenPlanMore1000_ShouldReturnSuccessFalse()
        {
            string stringMore = GenerateString(1001);

            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationSubmit = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = "TestSubmit",
                Plan = stringMore,
                Name = "TestSubmit"
            };

            var data = await handler.CreateApplication(applicationSubmit, CancellationToken.None);

            //Act
            var response = await handler.SubmitApplication(data.Data.Id, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }

        [Fact]
        public async Task SubmitApplication_WhenNameMore100_ShouldReturnSuccessFalse()
        {
            string stringMore = GenerateString(101);

            //Arrange
            var handler = new ApplicationСonferenceService(Mapper, UnitOfWork, new(), new());

            var applicationSubmit = new ApplicationСonferenceRequestDTO()
            {
                AuthorId = Guid.Parse("0fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Type = TypeOfActivity.Report,
                Description = "TestSubmit",
                Plan = "TestSubmit",
                Name = stringMore
            };

            var data = await handler.CreateApplication(applicationSubmit, CancellationToken.None);

            //Act
            var response = await handler.SubmitApplication(data.Data.Id, CancellationToken.None);

            //Assert
            Assert.False(response.Success);
        }
    }
}
