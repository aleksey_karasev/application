﻿

using ApplicationProject.Application.Common.Mappings;
using ApplicationProject.Persistence.Context;
using ApplicationProject.Persistence.Repositories;
using AutoMapper;

namespace ApplicationProject.Tests.Common
{
    /// <summary>
    /// Describes an inheritance class that creates a context for each inheritor
    /// </summary>
    public abstract class TestBase : IDisposable
    {
        public readonly UnitOfWork<ApplicationDbContext> UnitOfWork;
        public readonly ApplicationDbContext Context;
        public readonly IMapper Mapper;

        protected TestBase()
        {
            Context = ContextFactory.Create();
            UnitOfWork = new UnitOfWork<ApplicationDbContext>(Context);
            var configurationBuilder = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });

            Mapper = configurationBuilder.CreateMapper();
        }

        public void Dispose()
        {
            ContextFactory.Destroy(Context);
        }

        public string GenerateString(int length)
        {
            byte[] bytes = new byte[length];
            Random random = new Random();
            random.NextBytes(bytes);

            return Convert.ToBase64String(bytes);
        }
    }
}
