﻿
using ApplicationProject.Domain.Entities;
using ApplicationProject.Domain.Enums;
using ApplicationProject.Persistence.Context;
using Microsoft.EntityFrameworkCore;

namespace ApplicationProject.Tests.Common
{
    /// <summary>
    /// Describes a class for generating a virtual database for testing
    /// </summary>
    public class ContextFactory
    {
        public static Guid ApplicationIdForUpdate = Guid.Parse("1fa85f64-5717-4562-b3fc-2c963f66afa6");
        public static Guid ApplicationSubmitedIdForUpdate = Guid.Parse("2fa85f64-5717-4562-b3fc-2c963f66afa6");

        public static Guid ApplicationIdForDelete = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6");
        public static Guid ApplicationSubmitedIdForDelete = Guid.Parse("4fa85f64-5717-4562-b3fc-2c963f66afa6");

        public static Guid ApplicationIdForQuery = Guid.Parse("7fa85f64-5717-4562-b3fc-2c963f66afa6");
        public static Guid ApplicationSubmitedIdForQuery = Guid.Parse("8fa85f64-5717-4562-b3fc-2c963f66afa6");

        public static Guid UserIdQuery = Guid.Parse("7fa85f64-5717-4562-b3fc-2c963f66afa6");
        public static Guid UserIdSubmittedQuery = Guid.Parse("8fa85f64-5717-4562-b3fc-2c963f66afa6");

        public ApplicationDbContext Context { get; private set; }

        public static ApplicationDbContext Create()
        {

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString())
            .Options;

            var context = new ApplicationDbContext(options);
            context.Database.EnsureCreated();

            context = AddApplication(context);

            context.SaveChanges();

            return context;
        }

        public static void Destroy(ApplicationDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }

        private static ApplicationDbContext AddApplication(ApplicationDbContext context)
        {
            context.Applications.AddRange(
                new ApplicationСonference
                {
                    CreateDate = new DateTime(2010, 1, 1),
                    SubmitDate = null,
                    AuthorId = Guid.Parse("1fa85f64-5717-4562-b3fc-2c963f66afa6"),
                    Id = ApplicationIdForUpdate,
                    Type = TypeOfActivity.Report,
                    Description = "Test1",
                    Plan = "Test1",
                    Name = "TestApplication1"
                },
                new ApplicationСonference
                {
                    CreateDate = new DateTime(2010, 1, 1),
                    SubmitDate = new DateTime(2020, 2, 1),
                    AuthorId = Guid.Parse("2fa85f64-5717-4562-b3fc-2c963f66afa6"),
                    Id = ApplicationSubmitedIdForUpdate,
                    Type = TypeOfActivity.Report,
                    Description = "Test2",
                    Plan = "Test2",
                    Name = "TestApplication2"
                },
                new ApplicationСonference
                {
                    CreateDate = new DateTime(2010, 1, 1),
                    SubmitDate = null,
                    AuthorId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                    Id = ApplicationIdForDelete,
                    Type = TypeOfActivity.Report,
                    Description = "Test3",
                    Plan = "Test3",
                    Name = "TestApplication3"
                },
                new ApplicationСonference
                {
                    CreateDate = new DateTime(2010, 1, 1),
                    SubmitDate = new DateTime(2020, 2, 1),
                    AuthorId = Guid.Parse("4fa85f64-5717-4562-b3fc-2c963f66afa6"),
                    Id = ApplicationSubmitedIdForDelete,
                    Type = TypeOfActivity.Report,
                    Description = "Test4",
                    Plan = "Test4",
                    Name = "TestApplication4"
                },
                new ApplicationСonference
                {
                    CreateDate = new DateTime(2010, 1, 1),
                    SubmitDate = null,
                    AuthorId = UserIdQuery,
                    Id = ApplicationIdForQuery,
                    Type = TypeOfActivity.Report,
                    Description = "Test7",
                    Plan = "Test7",
                    Name = "TestApplication7"
                },
                new ApplicationСonference
                {
                    CreateDate = new DateTime(2010, 1, 1),
                    SubmitDate = new DateTime(2020, 2, 1),
                    AuthorId = UserIdSubmittedQuery,
                    Id = ApplicationSubmitedIdForQuery,
                    Type = TypeOfActivity.Report,
                    Description = "Test8",
                    Plan = "Test8",
                    Name = "TestApplication8"
                }
            );

            return context;
        }

    }
}
