﻿using AutoMapper;

namespace ApplicationProject.Application.Common.Mappings
{
    /// <summary>
    /// The interface is used for mapping heirs
    /// </summary>
    /// <typeparam name="T">Entity for mapping</typeparam>
    public interface ICreateMap<T>
    {
        public void Mapping(Profile profile) => profile.CreateMap(typeof(T), GetType()).ReverseMap();
    }
}
