﻿
using ApplicationProject.Application.DTOs.ApplicationСonferences;
using ApplicationProject.Application.Interfaces.Repositories;
using ApplicationProject.Application.Interfaces.Services;
using ApplicationProject.Application.Validation.ApplicationConference;
using ApplicationProject.Shared;

using AutoMapper;
using AutoMapper.QueryableExtensions;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;

namespace ApplicationProject.Domain.Entities
{
    public class ApplicationСonferenceService : IApplicationСonferenceService
    {
        private readonly IMapper _mapper;

        private readonly IRepository<ApplicationСonference> _applicationRepository;
        private readonly IUnitOfWork _unitOfWork;

        private readonly ApplicationСonferenceValidation _applicationValidator;
        private readonly ApplicationСonferenceSubmitValidation _applicationSubmitValidator;

        public ApplicationСonferenceService(IMapper mapper, IUnitOfWork unitOfWork,
               ApplicationСonferenceValidation applicationValidator,
               ApplicationСonferenceSubmitValidation applicationSubmitValidator)
        {
            _mapper = mapper;
            _applicationRepository = unitOfWork.GetRepository<ApplicationСonference>();
            _unitOfWork = unitOfWork;

            _applicationValidator = applicationValidator;
            _applicationSubmitValidator = applicationSubmitValidator;
        }

        public async Task<Result<ApplicationСonferenceResponseDTO>> CreateApplication(ApplicationСonferenceRequestDTO applicationDTO, CancellationToken cancellationToken)
        {
            string error = "A draft already exists for this user";

            //I'm checking against an existing draft, because you can only have 1 draft
            var applicationCurrent = await _applicationRepository.Entities
                .FirstOrDefaultAsync(f => f.AuthorId == applicationDTO.AuthorId
                                       && f.SubmitDate == null
                                       && f.CreateDate != null);

            if (applicationCurrent == null)
            {
                var valid = _applicationValidator.Validate(applicationDTO);

                if (valid.IsValid)
                {
                    var application = _mapper.Map<ApplicationСonference>(applicationDTO);

                    application.CreateDate = DateTime.UtcNow;

                    var applicationCreate = await _applicationRepository.AddAsync(application);

                    await _unitOfWork.SaveChangesAsync(cancellationToken);

                    var res = _mapper.Map<ApplicationСonferenceResponseDTO>(applicationCreate);

                    return Result<ApplicationСonferenceResponseDTO>.Ok(res, "Application success created");
                }

                error = valid.ToString();
            }

            return Result<ApplicationСonferenceResponseDTO>.Fail(error);
        }

        public async Task<Result<ApplicationСonferenceResponseDTO>> UpdateApplication(Guid id, ApplicationСonferenceRequestDTO applicationDTO, CancellationToken cancellationToken)
        {
            string error = "Application is not found/has already been submitted for review";

            var application = await _applicationRepository.GetAsync(id);

            if (application is not null && application.SubmitDate == null)
            {
                var valid = _applicationValidator.Validate(applicationDTO);

                if (valid.IsValid)
                {
                    application = SetNewDataApplicationСonference(application, applicationDTO);

                    var applicationUpdate = _applicationRepository.Update(application);

                    await _unitOfWork.SaveChangesAsync(cancellationToken);

                    var res = _mapper.Map<ApplicationСonferenceResponseDTO>(applicationUpdate);

                    return Result<ApplicationСonferenceResponseDTO>.Ok(res, "Application success updated");
                }

                error = valid.ToString();   
            }
            return Result<ApplicationСonferenceResponseDTO>.Fail(error);
        }

        private ApplicationСonference SetNewDataApplicationСonference(ApplicationСonference application, ApplicationСonferenceRequestDTO applicationDTO)
        {
            if (!Guid.Empty.Equals(applicationDTO.AuthorId)) { application.AuthorId = applicationDTO.AuthorId; }
            if (!string.IsNullOrWhiteSpace(applicationDTO.Name)) { application.Name = applicationDTO.Name; }
            if (!string.IsNullOrWhiteSpace(applicationDTO.Description)) { application.Description = applicationDTO.Description; }
            if (!string.IsNullOrWhiteSpace(applicationDTO.Plan)) { application.Plan = applicationDTO.Plan; }

            return application;
        }


        public async Task<Result<Guid>> DeleteApplication(Guid id, CancellationToken cancellationToken)
        {
            string error = "Application is not found/has already been submitted for review";

            var application = await _applicationRepository.GetAsync(id);

            if (application is not null && application.SubmitDate == null)
            {
                _applicationRepository.Delete(application);

                await _unitOfWork.SaveChangesAsync(cancellationToken);

                return Result<Guid>.Ok(id, "Application success deleted");
            }

            return Result<Guid>.Fail(error);
        }

        public async Task<Result<Guid>> SubmitApplication(Guid id, CancellationToken cancellationToken)
        {
            string error = "Application is not found/has already been submitted for review";

            var application = await _applicationRepository.GetAsync(id);

            if (application is not null && application.SubmitDate == null)
            {
                var valid = _applicationSubmitValidator.Validate(application);

                if (valid.IsValid)
                {
                    application.SubmitDate = DateTime.UtcNow;

                    _applicationRepository.Update(application);

                    await _unitOfWork.SaveChangesAsync(cancellationToken);

                    return Result<Guid>.Ok(id, "Application success submited");
                }

                error = valid.ToString();
            }
            return Result<Guid>.Fail(error);
        }

        public async Task<Result<ApplicationСonferenceResponseDTO>> GetApplicationById(Guid id)
        {
            var application = await _applicationRepository.GetAsync(id);

            if (application is not null)
            {
                var applicationDTO = _mapper.Map<ApplicationСonferenceResponseDTO>(application);

                return Result<ApplicationСonferenceResponseDTO>.Ok(applicationDTO, "Application found");
            }

            return Result<ApplicationСonferenceResponseDTO>.Fail("Application is not found");
        }

        public async Task<Result<IList<ApplicationСonferenceResponseDTO>>> GetApplicationsSubmittedAfter(DateTime submittedAfter)
        {
            submittedAfter = submittedAfter.Kind == DateTimeKind.Unspecified
                ? DateTime.SpecifyKind(submittedAfter, DateTimeKind.Utc)
                : submittedAfter.ToUniversalTime();

            var applicationList = _applicationRepository.Entities
                                                .Where(w => w.SubmitDate > submittedAfter);

            if (applicationList is not null)
            {
                var applicationDTOList = await applicationList
                                .ProjectTo<ApplicationСonferenceResponseDTO>(_mapper.ConfigurationProvider)
                                .ToListAsync();

                return Result<IList<ApplicationСonferenceResponseDTO>>.Ok(applicationDTOList, "Applications received successfully");
            }

            return Result<IList<ApplicationСonferenceResponseDTO>>.Fail("Applications is not found");
        }

        public async Task<Result<IList<ApplicationСonferenceResponseDTO>>> GetApplicationsUnsubmittedOlderThan(DateTime olderThan)
        {
            olderThan = olderThan.Kind == DateTimeKind.Unspecified
                ? DateTime.SpecifyKind(olderThan, DateTimeKind.Utc)
                : olderThan.ToUniversalTime();

            var applicationList = _applicationRepository.Entities
                                    .Where(w => w.SubmitDate == null && w.CreateDate < olderThan);

            if (applicationList is not null)
            {
                var applicationDTOList = await applicationList
                            .ProjectTo<ApplicationСonferenceResponseDTO>(_mapper.ConfigurationProvider)
                            .ToListAsync();

                return Result<IList<ApplicationСonferenceResponseDTO>>.Ok(applicationDTOList, "Applications received successfully");
            }

            return Result<IList<ApplicationСonferenceResponseDTO>>.Fail("Applications is empty");
        }

        public async Task<Result<ApplicationСonferenceResponseDTO>> GetCurrentApplicationForUser(Guid userId)
        {
            var application = await _applicationRepository.Entities
                                .FirstOrDefaultAsync(f => f.AuthorId == userId
                                                       && f.SubmitDate == null
                                                       && f.CreateDate != null);

            if (application is not null)
            {
                var applicationDTO = _mapper.Map<ApplicationСonferenceResponseDTO>(application);

                return Result<ApplicationСonferenceResponseDTO>.Ok(applicationDTO, "Application found");
            }

            return Result<ApplicationСonferenceResponseDTO>.Fail("Application is not found");
        }
    }
}
