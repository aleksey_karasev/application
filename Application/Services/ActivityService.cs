﻿using ApplicationProject.Application.DTOs.Activity;
using ApplicationProject.Application.DTOs.ApplicationСonferences;
using ApplicationProject.Application.Interfaces.Services;
using ApplicationProject.Domain.Enums;
using ApplicationProject.Shared;

namespace ApplicationProject.Domain.Entities
{
    public class ActivityService : IActivityService
    {
        public async Task<Result<IList<ActivityResponseDTO>>> GetActivityTypes()
        {
            var activityTypes = new List<ActivityResponseDTO>
            {
                new ActivityResponseDTO { Activity = TypeOfActivity.Report.ToString(), Description = "Доклад, 35-45 минут" },
                new ActivityResponseDTO { Activity = TypeOfActivity.Masterclass.ToString(), Description = "Мастеркласс, 1-2 часа" },
                new ActivityResponseDTO { Activity = TypeOfActivity.Discussion.ToString(), Description = "Дискуссия / круглый стол, 40-50 минут" }
            };

            return Result<IList<ActivityResponseDTO>>.Ok(activityTypes, "Activities received successfully");
        }
    }
}
