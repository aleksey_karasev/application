﻿
using System.Linq.Expressions;
using ApplicationProject.Domain.Abstractions;
using ApplicationProject.Shared;

namespace ApplicationProject.Application.Extensions
{
    /// <summary>
    /// The class that calls the basic filtering and sorting mechanisms
    /// </summary>
    public static class QueryableExtensions
    {
        public static IQueryable<T> ApplyFilters<T>(this IQueryable<T> query,
             BaseFilter filter, Dictionary<string, Expression<Func<T, bool>>> columns)
            where T : Entity
        {
            if (query == null || filter == null)
                return query;

            foreach (var column in columns)
            {
                var expression = column.Value;
                var hasValue = column.Key;

                if (!hasValue.Contains("None"))
                {
                    query = query.Where(expression);
                }
            }

            return query;
        }

        public static IQueryable<T> ApplySorting<T>(this IQueryable<T> query,
             BaseFilter sort, Dictionary<string, Expression<Func<T, object>>> columnsMap)
            where T : Entity
        {
            var normalizationSort = sort.SortBy?.ToLower();

            if (String.IsNullOrWhiteSpace(sort.SortBy) || !columnsMap.ContainsKey(normalizationSort))
                return query;

            if (sort.IsSortAscending)
                return query.OrderBy(columnsMap[normalizationSort]);
            else
                return query.OrderByDescending(columnsMap[normalizationSort]);
        }


        public static Dictionary<string, Expression<Func<T, bool>>> GetBaseFilterDictionary<T>(BaseFilter filter) where T : Entity
        {
            var columnsFilter = new Dictionary<string, Expression<Func<T, bool>>>();

            return columnsFilter;
        }

        public static Dictionary<string, Expression<Func<T, object>>> GetBaseQueryDictionary<T>() where T : Entity
        {
            var columnsOrder = new Dictionary<string, Expression<Func<T, object>>>();

            return columnsOrder;
        }

    }
}
