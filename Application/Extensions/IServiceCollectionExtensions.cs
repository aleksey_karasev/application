﻿using ApplicationProject.Application.Interfaces.Services;
using ApplicationProject.Domain.Entities;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace ApplicationProject.Application.Extensions
{
    /// <summary>
    /// The class for connecting services from the application layer
    /// </summary>
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddAutoMapper()
                    .AddValidators()
                    .AddServices();

            return services;
        }

        private static IServiceCollection AddAutoMapper(this IServiceCollection services)
        {
            var assembly = Assembly.GetExecutingAssembly();
            services.AddAutoMapper(assembly);

            return services;
        }

        private static IServiceCollection AddValidators(this IServiceCollection services)
        {
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());

            return services;
        }

        private static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IActivityService, ActivityService>()
                    .AddScoped<IApplicationСonferenceService, ApplicationСonferenceService>();

            return services;
        }
    }
}
