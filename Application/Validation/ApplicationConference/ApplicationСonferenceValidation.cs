﻿
using ApplicationProject.Application.DTOs.ApplicationСonferences;
using FluentValidation;

namespace ApplicationProject.Application.Validation.ApplicationConference
{
    public class ApplicationСonferenceValidation : AbstractValidator<ApplicationСonferenceRequestDTO>
    {
        public ApplicationСonferenceValidation()
        {
            RuleFor(x => x)
                .NotNull().WithMessage("Application is null");

            RuleFor(x => x.AuthorId)
                .NotNull().WithMessage("AuthorId is null")
                .NotEqual(Guid.Empty).WithMessage("AuthorId must be empty");

            RuleFor(x => x)
                .Must(HaveAdditionalField)
                .WithMessage("At least one additional field (Name, Description, Plan) is required");
            
        }

        private bool HaveAdditionalField(ApplicationСonferenceRequestDTO dto)
        {
            //Check if there is at least one additional field besides AuthorId
            return !string.IsNullOrWhiteSpace(dto.Name)
                || !string.IsNullOrWhiteSpace(dto.Description)
                || !string.IsNullOrWhiteSpace(dto.Plan);
        }
    }
}
