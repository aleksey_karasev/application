﻿
using ApplicationProject.Domain.Entities;
using FluentValidation;

namespace ApplicationProject.Application.Validation.ApplicationConference
{
    public class ApplicationСonferenceSubmitValidation : AbstractValidator<ApplicationСonference>
    {
        public ApplicationСonferenceSubmitValidation()
        {
            RuleFor(x => x)
                .NotNull().WithMessage("Application is null");

            RuleFor(x => x.AuthorId)
                .NotNull().WithMessage("AuthorId is null")
                .NotEqual(Guid.Empty).WithMessage("AuthorId must be empty");

            RuleFor(x => x.Name)
                .NotEmpty().NotNull().WithMessage("Name is null")
                .MaximumLength(100).WithMessage("Max Length Name is 100");

            RuleFor(x => x.Description)
                .MaximumLength(300).WithMessage("Max Length Description is 300");

            RuleFor(x => x.Plan)
                .NotEmpty().NotNull().WithMessage("Plan is null")
                .MaximumLength(1000).WithMessage("Max Length Plan is 1000");

        }

    }
}
