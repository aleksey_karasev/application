﻿
using ApplicationProject.Application.DTOs.Activity;
using ApplicationProject.Shared;

namespace ApplicationProject.Application.Interfaces.Services
{
    public interface IActivityService
    {
        public Task<Result<IList<ActivityResponseDTO>>> GetActivityTypes();
    }
}
