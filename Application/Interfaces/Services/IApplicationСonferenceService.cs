﻿
using ApplicationProject.Application.DTOs.ApplicationСonferences;
using ApplicationProject.Shared;

namespace ApplicationProject.Application.Interfaces.Services
{
    public interface IApplicationСonferenceService
    {
        Task<Result<ApplicationСonferenceResponseDTO>> CreateApplication(ApplicationСonferenceRequestDTO applicationDTO, CancellationToken cancellationToken);
        Task<Result<ApplicationСonferenceResponseDTO>> UpdateApplication(Guid id, ApplicationСonferenceRequestDTO applicationDTO, CancellationToken cancellationToken);
        Task<Result<Guid>> DeleteApplication(Guid id, CancellationToken cancellationToken);
        Task<Result<Guid>> SubmitApplication(Guid id, CancellationToken cancellationToken);
        Task<Result<IList<ApplicationСonferenceResponseDTO>>> GetApplicationsSubmittedAfter(DateTime submittedAfter);
        Task<Result<IList<ApplicationСonferenceResponseDTO>>> GetApplicationsUnsubmittedOlderThan(DateTime olderThan);
        Task<Result<ApplicationСonferenceResponseDTO>> GetCurrentApplicationForUser(Guid userId);
        Task<Result<ApplicationСonferenceResponseDTO>> GetApplicationById(Guid id);
    }
}
