﻿using ApplicationProject.Domain.Abstractions;

namespace ApplicationProject.Application.Interfaces.Repositories
{
    /// <summary>
    /// Describes abstract work with context
    /// </summary>
    /// <typeparam name="T">Essence</typeparam>
    public interface IRepository<T>
        where T : IEntity
    {
        IQueryable<T> Entities { get; }

        #region "Get"

        IQueryable<T> GetAll();

        Task<ICollection<T>> GetAllAsync();

        T Get(Guid id);

        Task<T> GetAsync(Guid id);

        #endregion

        #region "Delete"

        void Delete(T entity);

        #endregion

        #region "Update"

        T Update(T entity);

        #endregion

        #region "Add"

        T Add(T entity);

        Task<T> AddAsync(T entity);

        void AddRange(ICollection<T> entities);

        Task AddRangeAsync(ICollection<T> entities);

        #endregion

    }
}
