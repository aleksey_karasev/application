﻿using ApplicationProject.Domain.Abstractions;

namespace ApplicationProject.Application.Interfaces.Repositories
{
    /// <summary>
    /// Describes the abstraction of the implementation of the IUnitOfWork pattern
    /// </summary>
    public interface IUnitOfWork
    {
        IRepository<T> GetRepository<T>() where T : Entity;

        #region "SaveChanges"

        int SaveChanges(CancellationToken cancellationToken);

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        #endregion
    }
}
