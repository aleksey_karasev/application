﻿using ApplicationProject.Application.Common.Mappings;
using ApplicationProject.Domain.Entities;
using ApplicationProject.Domain.Enums;

namespace ApplicationProject.Application.DTOs.ApplicationСonferences
{
    public class ApplicationСonferenceResponseDTO : ICreateMap<ApplicationСonference>
    {
        public Guid Id { get; set; }

        public Guid AuthorId { get; set; }

        public TypeOfActivity Type { get; set; }

        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? Plan { get; set; }
    }
}
