﻿using ApplicationProject.Domain.Abstractions;

namespace ApplicationProject.Application.DTOs.Activity
{
    public class ActivityResponseDTO
    {
        public string? Activity { get; set; }
        public string? Description { get; set; }
    }
}
