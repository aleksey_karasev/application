﻿using ApplicationProject.Application.DTOs.ApplicationСonferences;
using ApplicationProject.Application.Interfaces.Services;
using ApplicationProject.Shared;
using Microsoft.AspNetCore.Mvc;

namespace ApplicationProject.Web.Controllers
{
    [ApiController]
    public class ApplicationConferenceController : Controller
    {
        private readonly IApplicationСonferenceService _applicationСonferenceService;

        public ApplicationConferenceController(IApplicationСonferenceService applicationСonferenceService)
        {
            _applicationСonferenceService = applicationСonferenceService;
        }

        [HttpPost("applications")]
        public async Task<Result<ApplicationСonferenceResponseDTO>> CreateApplication(ApplicationСonferenceRequestDTO applicationDTO, CancellationToken cancellationToken)
        {
            return await _applicationСonferenceService.CreateApplication(applicationDTO, cancellationToken);
        }

        [HttpPut("applications/{id}")]
        public async Task<Result<ApplicationСonferenceResponseDTO>> UpdateApplication(Guid id, ApplicationСonferenceRequestDTO applicationDTO, CancellationToken cancellationToken)
        {
            return await _applicationСonferenceService.UpdateApplication(id, applicationDTO, cancellationToken);
        }

        [HttpDelete("applications/{id}")]
        public async Task<Result<Guid>> DeleteApplication(Guid id, CancellationToken cancellationToken)
        {
            return await _applicationСonferenceService.DeleteApplication(id, cancellationToken);
        }

        [HttpPost("applications/{id}/submit")]
        public async Task<Result<Guid>> SubmitApplication(Guid id, CancellationToken cancellationToken)
        {
            return await _applicationСonferenceService.SubmitApplication(id, cancellationToken);
        }

        [HttpGet("applications")]
        public async Task<Result<IList<ApplicationСonferenceResponseDTO>>> GetApplications(DateTime? submittedAfter, DateTime? unsubmittedOlder)
        {
            if (submittedAfter.HasValue && !unsubmittedOlder.HasValue)
            {
                return await _applicationСonferenceService.GetApplicationsSubmittedAfter(submittedAfter.Value);
            }
            else if (unsubmittedOlder.HasValue && !submittedAfter.HasValue)
            {
                return await _applicationСonferenceService.GetApplicationsUnsubmittedOlderThan(unsubmittedOlder.Value);
            }
            else
            {
                return Result<IList<ApplicationСonferenceResponseDTO>>.Fail("Enter one of the dates");
            }
        }

        [HttpGet("applications/{id}")]
        public async Task<Result<ApplicationСonferenceResponseDTO>> GetApplicationById(Guid id)
        {
            return await _applicationСonferenceService.GetApplicationById(id);
        }

        [HttpGet("users/{authorId}/currentapplication")]
        public async Task<Result<ApplicationСonferenceResponseDTO>> GetCurrentApplicationForUser(Guid authorId)
        {
            return await _applicationСonferenceService.GetCurrentApplicationForUser(authorId);
        }
    }
}
