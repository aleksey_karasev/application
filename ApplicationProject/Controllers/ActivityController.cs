﻿using ApplicationProject.Application.DTOs.Activity;
using ApplicationProject.Application.DTOs.ApplicationСonferences;
using ApplicationProject.Application.Interfaces.Services;
using ApplicationProject.Shared;
using Microsoft.AspNetCore.Mvc;

namespace ApplicationProject.Web.Controllers
{
    [ApiController]
    public class ActivityController : Controller
    {
        private readonly IActivityService _activityService;

        public ActivityController(IActivityService activityService)
        {
            _activityService = activityService;
        }


        [HttpGet("activities")]
        public async Task<Result<IList<ActivityResponseDTO>>> GetApplications()
        {
            return await _activityService.GetActivityTypes();
        }
    }
}
